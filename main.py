#-*- coding: utf-8 -*-

import tornado.ioloop
from tornado.web import ErrorHandler, RequestHandler, RedirectHandler, Application
from profileonline import ProfileHandler

class MainHandler(RequestHandler):
    def get(self):
        li = []
        for i in xrange(2):
            li.append(i)
        self.write('Hello, world!')

ProfileHandler.set_profile_function(MainHandler.write,
                                    'arg is {1}, time is {axetime}')

class PostHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('<html><body><form action="/post" method="post">'
                   '<input type="text" name="message">'
                   '<input type="submit" value="Submit">'
                   '</form></body></html>')

    def post(self):
        self.set_header("Content-Type", "text/plain")
        self.write("You wrote " + self.get_argument("message"))

def authentication(request, uri):
    #print request
    return True

application = tornado.web.Application([
    (r'/', MainHandler),
    (r'/profile(/.*)', ProfileHandler, dict(auth=authentication)),
    (r'/redirect', RedirectHandler, {'url':'/', 'permanent':False}),
    (r'/post', PostHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
