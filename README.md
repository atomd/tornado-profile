Now you can only try the console mode.

You can add ProfileHandler in your project:

    from online_profile import ProfileHandler

    application = tornado.web.Application([
        (r'/profile', ProfileHandler),
        (r'/', MainHandler),
        (r'/redirect', RedirectHandler, {'url':'/', 'permanent':False}),
        (r'/post', PostHandler),
        ])

Now you can visit:

* http://domain:port/profile/?json=1
* http://domain:port/profile/post?fullpath=1
* http://domain:port/profile/redirect?json=1&fullpath=1
