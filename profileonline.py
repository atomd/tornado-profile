#!/usr/bin/env python

import inspect
import urlparse
import types
import pstats
import functools
import cProfile as profile
from contextlib import contextmanager

from tornado import escape
from tornado.web import ErrorHandler, RequestHandler
from tornado.escape import native_str, parse_qs_bytes
from tornado.util import b

from profiletimer import get_time

__all__ = ['ProfileHandler']

def _finish(handler, chunk=None):
    """ This method is modified from web.py - RequestHandler - finish
    Finishes this response, ending the HTTP request.
    NOTE:
        * Remove the action - compute Etags
          We don't need Etag(maybe return 304 code), and Content-Length.
        * Remove the action - set_close_callback(none)
          Prohibit operating the stream.
        * Remove the action - request.finish()
          The method maybe close the connection.
    """
    if handler._finished:
        raise RuntimeError("finish() called twice.  May be caused "
                            "by using async operations without the "
                            "@asynchronous decorator.")
    if chunk is not None: handler.write(chunk)
    if not handler.application._wsgi:
        handler.flush(include_footers=True)
        handler._log()

def _flush(handler, include_footers=False, callback=None):
    """
    This function is modified from web.py - RequestHandler - flush
    Flushes the current output buffer to handler.response_buffer
    NOTE:
        * All flushed data can be written to handler.response_buffer
          immediately, so evenytime callback will call be run.
        * Remove the transforms(Chunked, Gzip), it is useless.
        * In profile mode, HEAD request does not need to be supported.
    """
    if handler.application._wsgi:
        raise Exception("WSGI applications do not support flush()")

    handler.response_buffer.extend(handler._write_buffer)
    handler._write_buffer = []
    if callback is not None:
        callback()

def _call(application, request):
    """
    This function is modified from web.py Application __call__
    Every time application re-create handler in order to execute request.
    NOTE:
        * Remove the comments.
        * Clear the list of transforms.
        * Change the method flush and finish dynamically.
        * Create attribute response body for handler.
        * Return (response_buffer, headers, list_headers, cookies)
    """
    transforms = []
    handler = None
    handler_class = None
    args = []
    kwargs = {}
    handlers = application._get_host_handlers(request)
    if not handlers:
        handler = RedirectHandler(
            application,
            request,
            url="http://" + application.default_host + "/")
    else:
        for spec in handlers:
            match = spec.regex.match(request.path)
            if match:
                handler = spec.handler_class(application,
                                             request,
                                             **spec.kwargs)
                handler_class = spec.handler_class
                if spec.regex.groups:
                    def unquote(s):
                        if s is None: return s
                        return escape.url_unescape(s, encoding=None)
                    if spec.regex.groupindex:
                        kwargs = dict(
                            (k, unquote(v))
                            for (k, v) in match.groupdict().iteritems())
                    else:
                        args = [unquote(s) for s in match.groups()]
                break
        if not handler:
            handler = ErrorHandler(application,
                                   request,
                                   status_code=404)
            handler_class = ErrorHandler

        if application.settings.get("debug"):
            if getattr(RequestHandler, "_templates", None):
                for loader in RequestHandler._templates.values():
                    loader.reset()
            RequestHandler._static_hashes = {}

        try:
            flush, handler_class.flush = handler_class.flush, _flush
            finish, handler_class.finish = handler_class.finish, _finish
            handler.response_buffer = []
            handler._execute(transforms, *args, **kwargs)
        finally:
            handler_class.flush = flush
            handler_class.finish = finish

        return (handler.response_buffer,
                handler._headers,
                handler._list_headers,
                getattr(handler, "_new_cookies", []))


class ProfileHandler(RequestHandler):

    PROFILE_FUNC_LIST = []
    EXCLUDE_HEADER_KEYS = ('Connection', 'Content-Type', 'Content-Length',
                           'Last-Modified', 'Expires', 'Cache-Control')

    def initialize(self, auth=None, sort=None, fullpath=False):
        self.profile = profile.Profile()
        self.sort = sort or ('time', 'calls', 'cumulative')
        self.fullpath = fullpath
        self.auth = auth
        self.funcs = {}

    @classmethod
    def set_profile_function(cls, func, logformat):
        if not (inspect.isfunction(func) or inspect.ismethod(func)):
            raise TypeError('function is not method or function')
        cls.PROFILE_FUNC_LIST.append((func, logformat))

    def _wrapper_func(self, func):
        loglist = []
        self.funcs[func] = loglist

        @functools.wraps(func)
        def _func(*args, **kw):
            t = get_time()
            res = func(*args, **kw)
            timecost = get_time() - t
            loglist.append((args, kw, timecost))
            return res
        return _func

    def _change_function(self, nowrapper):
        for item in self.PROFILE_FUNC_LIST:
            func = item[0]
            new = func if nowrapper else self._wrapper_func(func)

            if inspect.isfunction(func):
                func.func_globals[func.func_name] = new
            elif inspect.ismethod(func):
                if func.im_self is None:
                    setattr(func.im_class, func.im_func.func_name, new)
                else:
                    setattr(func.im_self, func.im_func.func_name, new)

    @contextmanager
    def _request_context(self):
        try:
            self._change_function(nowrapper=False)
            yield
        finally:
            self._change_function(nowrapper=True)

    def _change_request(self, url):
        """
        According to the parameters url to modified the request.
        """
        self.request.path = url
        index = self.request.uri.find(url)
        if index != -1:
            self.request.uri = self.request.uri[index:]

    def _subrequest(self, url):

        json = self.get_argument('json', None)
        if self.get_argument('fullpath', None) is not None:
            self.fullpath = True

        if url == '/test':
        #if not self.request.headers.get("Tornado-On-Profile"):
            if self.request.method == 'GET':
                return self.render("static/js_intercep.html")
            else:
                raise HTTPError(404)

        self._change_request(url)
        #get the application object on the stack
        application = inspect.getargvalues(
                inspect.stack()[3][0]
                ).locals['self']

        with self._request_context():
            buf, headers, list_headers, cookies = self.profile.runcall(
                    _call, application, self.request)

        self._update_headers(headers, list_headers, cookies)

        res = {}
        stats = pstats.Stats(self.profile)
        if not self.fullpath:
            stats.strip_dirs()
        stats = stats.sort_stats(*self.sort)

        if json is None:
            print >> self, 'URL : %s\n' % url
            self._print_funcs_list()
            stats.stream = self
            stats.print_stats()
            self.set_header('Content-Type',
                            'text/plain; charset=UTF-8')
            return self.finish()

        res['profile'] = {}
        res['profile']['functions'] = self._funcs_list()
        res['profile']['page'] = self._stats_dict(stats)
        res['response'] = b('').join(buf)
        return self.finish(res)

    def _update_headers(self, headers, list_headers, cookies):
        self._new_cookies = cookies
        for k, v in headers.items():
            if k not in self.EXCLUDE_HEADER_KEYS:
                if k == 'Location':
                    k = 'Profile-Location'
                self.set_header(k, v)
        for k, v in list_headers:
            if k not in self.EXCLUDE_HEADER_KEYS + ('Location',):
                self.add_header(k, v)

    @staticmethod
    def _ff(x):
        return '%.5f' % x

    def _print_funcs_list(self):
        for function, logformat in self.PROFILE_FUNC_LIST:
            print >> self, ' ' * 8, "function %s.%s (%d calls)\n" % (
                                        function.__module__,
                                        function.__name__,
                                        len(self.funcs[function] ))
            tottime = 0
            bufs = []
            logformat = '' * 2 + logformat
            for args, kw, time in sorted(self.funcs[function],
                                            reverse = True,
                                            key=lambda x : x[2]):
                tottime = tottime + time
                t = self._ff(time)
                bufs.append(logformat.format(*args, axetime=t, **kw))

            print >> self, ' ' * 2, ('Ordered by: execution time, '
                            'total time is %s.\n') % self._ff(tottime)
            for buf in bufs:
                print >> self, ' ' * 2, buf
            print >> self, '\n'

    def _funcs_list(self):
        funcs = []
        for function, logformat in self.PROFILE_FUNC_LIST:
            func = {}
            func['func_name'] = "%s.%s" % (function.__module__,
                                        function.__name__)
            func['total_calls'] = len(self.funcs[function])
            func['data'] = []
            funcs.append(func)
            tottime = 0
            for args, kw, time in sorted(self.funcs[function],
                                            reverse = True,
                                            key=lambda x : x[2]):
                tottime = tottime + time
                t = self._ff(time)
                log = logformat.format(*args, axetime=t, **kw)
                func['data'].append((t, log))
            func['total_time'] = self._ff(tottime)
        return funcs

    def _stats_dict(self, stats):

        title = ( 'ncalls', 'tottime', 'percall', 'cumtime', 'percall',
                  'filename:lineno(function)')

        profile = []
        for func in stats.fcn_list:
            cc, nc, tt, ct, callers = stats.stats[func]
            cct = str(nc)
            if nc != cc:
                cct = cct + '/' + str(cc)
            ttt = self._ff(tt)
            if nc == 0:
                nct = ''
            else:
                nct = self._ff(float(tt)/nc)
            ctt = self._ff(ct)
            if cc == 0:
                prt = ''
            else:
                prt = self._ff(float(ct)/cc)
            profile.append((cct, ttt, nct, ctt, prt,
                            pstats.func_std_string(func)))
        stats_dict = {  'total_calls' : stats.total_calls,
                        'prim_calls'  : stats.prim_calls,
                        'total_time'  : stats.total_tt,
                        'sort_type'   : stats.sort_type,
                        'data_name'   : title,
                        'data'        : profile }
        return stats_dict

    def get(self, url):
        self._auth(url)
        self._subrequest(url)

    def post(self, url):
        self._auth(url)
        self._subrequest(url)

    def delete(self, url):
        self._auth(url)
        self._subrequest(url)

    def put(self, url):
        self._auth(url)
        self._subrequest(url)

    def _auth(self, url):
        if self.auth is not None:
            if not self.auth(self, url):
                raise HTTPError(404)
