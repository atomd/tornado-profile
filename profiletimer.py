#! /usr/bin/env python

import os
import time

__all__ = ['get_time', 'timer']

get_time = None
timer = None

if hasattr(os, "times"):
    def _get_time_times(timer=os.times):
        t = timer()
        return t[0] + t[1]
_has_res = 0
try:
    import resource
    resgetrusage = lambda: resource.getrusage(resource.RUSAGE_SELF)
    def _get_time_resource(timer=resgetrusage):
        t = timer()
        #print '[debug]', t
        return t[0] + t[1]
    _has_res = 1
except ImportError:
    pass

if _has_res:
    timer = resgetrusage
    get_time = _get_time_resource
elif hasattr(time, 'clock'):
    timer = get_time = time.clock
elif hasattr(os, 'times'):
    timer = os.times
    get_time = _get_time_times
else:
    timer = get_time = time.time
